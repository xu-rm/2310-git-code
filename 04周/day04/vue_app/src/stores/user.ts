import { ref } from "vue";
import { defineStore } from "pinia";

export const useUserStore = defineStore("user", () => {
  // 存储用户登录状态
  const loginState = ref(window.localStorage.getItem("true") === "true");

  // 修改用户登录状态
  function setLoginState(type: boolean) {
    loginState.value = type;
  }

  // 存储用户id
  const userId = ref(window.localStorage.getItem("userid") || "");

  // 修改用户id
  function setUserId(id: string) {
    userId.value = id;
  }

  return { loginState, setLoginState, userId, setUserId };
});

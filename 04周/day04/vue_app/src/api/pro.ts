import ajax from '@/utils/request'

// 获取商品详情
export function getDetailList(id:string){
    return ajax({
        url: '/pro/detail/'+id,
    })
}

// 获取分类列表
export function getCategoryList(){
    return ajax({
        url: '/pro/categorylist',
    })
}


// 获取分类下品牌列表
export function getCategoryBrandList(data: { category: string }) {
    return ajax({
        url: "/pro/categorybrandlist",
        data,
    });
}

// 商品分类下某品牌列表的所有产品
interface IGetCategoryBrandProList {
    category: string;
    brand: string;
    count?: number;
    limitNum?: number;
}
export function getCategoryBrandProList(data: IGetCategoryBrandProList) {
    return ajax({
        url: "/pro/categorybrandprolist",
        data,
    });
}
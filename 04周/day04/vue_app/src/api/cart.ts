import ajax from '@/utils/request'

interface CartItem{
    userid:string,
    proid:string,
    num:string
}

// 查询购物车

export function getCart(data:{userid:string}){
    return ajax({
        url:"/cart/list",
        method:"POST",
        data
    })
}

// 添加购物车
export function addCart(data:CartItem){
    return ajax({
        url:"/cart/add",
        method:"POST",
        data

    })
}
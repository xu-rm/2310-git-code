import { createRouter, createWebHashHistory } from "vue-router";
import home from "../views/home/index.vue";
const routes = [
  {
    path: "/",
    name: "home",
    component: home,
  },
  {
    path: "/kind",
    name: "kind",
    component: () => import("@/views/kind/index.vue"),
  },
  {
    path: "/cart",
    name: "cart",
    component: () => import("@/views/cart/index.vue"),
  },
  {
    path: "/my",
    name: "my",
    component: () => import("@/views/my/index.vue"),
  },
  {
    path: "/login",
    name: "login",
    component: () => import("@/views/login/index.vue"),
    meta: {
      hidden: true,
    },
  },
  {
    path: "/register",
    name: "register",
    component: () => import("@/views/register/index.vue"),
    meta: {
      hidden: true,
    },
    children: [
      {
        path: "no1",
        name: "no1",
        component: () => import("@/views/register/no1.vue"),
      },
      {
        path: "no2",
        name: "no2",
        component: () => import("@/views/register/no2.vue"),
      },
      {
        path: "no3",
        name: "no3",
        component: () => import("@/views/register/no3.vue"),
      },
    ],
  },
];

const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL),
  routes,
});

export default router;

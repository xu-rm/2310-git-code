import ajax from "@/utils/request";

export function getBannerList(){
    return ajax({
        url:'/banner/list'
    })
}
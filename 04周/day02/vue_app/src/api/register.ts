import ajax from '@/utils/request'
//验证手机号
export function docheckphone(data : {tel:string}){
   return ajax({
    url:'/user/docheckphone',
    method:'POST',
    data
   })
}

// 获取验证码

export function dosendmsgcode (data:{tel:string}){
   return ajax({
      url:'/user/dosendmsgcode',
      method:'POST',
      data
   })
}

// 发送验证码
export function docheckcode(data:{tel:string,telcode:string}){
   return ajax({
      url:'/user/docheckcode',
      method:'POST',
      data
   })
}

// 注册页，校验手机号和密码

export function dofinishregister(data:{tel:string,password:string}){
   return ajax({
      url:'/user/dofinishregister',
      method:'POST',
      data
   })
}

import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// 自动引入组件
import Components from 'unplugin-vue-components/vite';
// 自动导入样式的解析器
import { VantResolver } from '@vant/auto-import-resolver';
// https://vitejs.dev/config/

// npm i @vant/auto-import-resolver unplugin-vue-components -D
export default defineConfig({
  plugins: [
    vue(),
    Components({
      resolvers: [VantResolver()],
    })
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  }
})

// const arr: number[] = [1, 2, 3];

// const arr2: Array<string> = ["aaa", "bbb"];

// const arr3: (number | string)[] = ["1", 2];

// console.log('arr: ',arr);
// console.log('arr2: ',arr2);
// console.log('arr3: ',arr3);


// const arr4: { name: string; age: number }[] = [
//   {
//     name: "zs",
//     age: 18,
//   },
//   {
//     name: "lis",
//     age: 20,
//   }
// ];

// console.log('arr4: ',arr4);


// interface IArr {name:string,age:number}[]

// const arr5:Array<IArr> = [{
//     name: "zs2",
//     age: 28,
//   },
//   {
//     name: "lis2",
//     age: 30,
//   }]

// console.log('arr4: ',arr5);

// const arr6:IArr[] = [{
//     name: "zs3",
//     age: 38,
//   },
//   {
//     name: "lis3",
//     age: 40,
//   }]

//   console.log('arr6: ',arr6);
  

// const arr: number[] =[1,2,3]

// console.log(arr);

// const arr2: any[] =[1,2,3,'1222333']

// console.log(arr2);

// const arr3:(number|string)[] = [1,2,'123']

// console.log(arr3);

// const arr4 : {
//   name:string
//   age:number

// }[] = [
//   {
//     name:'李四',
//     age:14
// },
// {
//   name:'王五',
//   age:15
// }

// ]
// console.log(arr4);

// interface IArr {
//   name:string
//   age:number
// }

// const arr5: IArr[] = [
//   {
//     name:'赵六',
//     age:16
//   }]

//   console.log(arr5);
  
// 泛型


const arr6:Array<number> = [1,2,3]

console.log(arr6);


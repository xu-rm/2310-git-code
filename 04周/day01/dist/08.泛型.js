// // function createArr(length: any, val: any): any[] {
// //   let arr: any[] = [];
// //   for (let i = 0; i < length; i++) {
// //     arr.push(val);
// //   }
// //   return arr;
// // }
// // console.log(createArr(4, "xf001"));
// function createArr<T>(length: number, val: T): T[] {
//   let arr: T[] = [];
//   for (let i = 0; i < length; i++) {
//     arr.push(val);
//   }
//   return arr;
// }
// // const arr1 = createArr<string>(3,'xf002')
// // console.log(arr1);
// interface IObj {
//   name: string;
//   age: number;
// }
// const arr2 = createArr<IObj>(6, {
//   name: "啦啦",
//   age: 18,
// });
// console.log(arr2);
function createArr(length, value) {
    var arr = [];
    for (var i = 0; i < length; i++) {
        arr.push(value);
    }
    return arr;
}
console.log(createArr(3, '123'));

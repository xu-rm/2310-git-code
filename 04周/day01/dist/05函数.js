//  function fn ():void {
//     console.log('没有返回值的函数');
//  }
//  fn()
// function fn2():number{
//     return 123
// }
// console.log('我是fn2: ',fn2());
// function fn3():any{
//     return {
//         name:'zsss',
//         age:66
//     }
// }
// console.log(fn3());
// function fn4(a:string):string{
//     return a
// }
// console.log(fn4('456'));
//返回一个对象
// function fn5():{name:string,age:number}{
//     return {
//         name:'综上所述',
//         age:11
//     }
// }
// console.log(fn5());
//使用接口返回一个对象类型
// interface IObj {
//     name:string,
//     age:number
// }
// function fn6():IObj{
//     return {
//         name:'啦啦啦啦',
//         age:88
//     }
// }
// console.log(fn6());
//使用type类型定义的类型作为函数的返回值
// type TObj = [{
//     name:string,
//     age:number
// }]
// function fn7(): TObj{
//     return [{
//         name:'啦啦啦',
//         age:66
//     }]
// }
// console.log(fn7());
//赋值式函数没有返回值的写法
//  const fn8:()=>void = function():void{
//     console.log(123);
//  }
//  fn8()
// const fn9:()=>string = function ():string{
//     return '123'
// }
// console.log(fn9());
// const fn10:(a:number,b:number)=>number = function(a:number,b:number):number{
//     return a+b
// }
// console.log(fn10(11,12));
// interface IObj {
//     name:string,
//     age:number
// }
// const fn11:()=>IObj = function():IObj{
//     return{
//         name:'哈哈哈',
//         age:87
//     }
// }
// console.log(fn11());
// const obj2 = {
//     name:'呀呀呀',
//     age:66
// }
// const fn12: (IObj: any) => IObj = function(IObj):IObj{
//     return IObj
// }
// console.log(fn12(obj2));
// 声明式函数
// 无参无返
// function fn(): void {}
// console.log(fn());
// function fn1(): void {
//   return undefined;
// }
// console.log(fn1());
// function fn2(): undefined {
//   return undefined;
// }
// console.log(fn2());
// 无参有返
// function fn3(): number {
//   return 100;
// }
// function fn4(): {
//   name: string;
//   age: number;
// } {
//   return { name: "张三", age: 12 };
// }
// console.log(fn4());
// interface IObj {
//     name:string
//     age:number
// }
// function fn5():IObj{
//     return {
//         name:'zzzz',
//         age:19
//     }
// }
// console.log(fn5());
// 有参无返
// function fn6(a: number, b: number): void {
//   console.log(a + b);
// }
// fn6(3, 4)
// 有参有返
// function fn7(a: number, b: number): number {
//   return a + b;
// }
// 赋值式函数
// 无参无返
// const fn8:()=>void = function():void{}
// 无参有返
// const fn9: () => number = function (): number {
//   return 100;
// };
// console.log(fn9());
// 有参无返
// const fn10: (a: number, b: number) => void = function (
//   a: number,
//   b: number
// ): void {
//     console.log(a,b);
// };
// fn10(1,2)
// 有参有返
// const fn11: (a: number, b: number) => number = function (
//   a: number,
//   b: number
// ): number {
//   return a + b;
// };
// console.log(fn11(2,3));
// 赋值式函数的箭头函数的有参有返
var fn12 = function (a, b) {
    return a + b;
};
console.log(fn12(6, 7));

// // type TDir = 'up' | 'down' | 'left' | 'right'
// // const up:TDir = 'up'
// // const down:TDir = 'down'
// // const left:TDir = 'left'
// // const right:TDir = 'right'
// // console.log('up:  ',up);
// // console.log('down:  ',down);
// // console.log('left:  ',left);
// // console.log('right:  ',right);
// type TObj = {
//     name:string
//     age:number
// }
// const obj1:TObj ={
//     name:'哈哈',
//     age:16
// }
// console.log('TObj:',obj1);
// interface IObj {
//     name:string
//     age:number
// }
// const obj2:IObj ={
//     name:'丫丫',
//     age:13
// }
// console.log('IObj: ',obj2);
// type 一般用于给字符串指定类型
// interface 一般用于给对象指定类型
// type TDir = "right" | "left" | "top" | "bottom";
// let dir: TDir = "top";
// dir = 'left'
// console.log(dir);

// interface IObject {
//     id:string
//     name:string
//     age:number
// }

// const obj:IObject = {
//     id:'001',
//     name:'王五',
//     age:18
// }

// console.log('他的名字是： ',obj.name);


// const obj:{
//     name:string
//     age:number
// } = {
//     name:'张三',
//     age:12
// }

// console.log(obj);

// interface IObj {
//     name:string,
//     age:number
// }

// const obj:IObj = {
//     name:'张三',
//     age:12
// }

// console.log(obj);


// 接口的嵌套

interface IObj {
    name:string
    age:number
    info:{
        weight:string
        height:string
        bo:boolean
    }
}

const obj = {
    name:'xrm',
    age:13,
    info:{
        weight:'50kg',
        height:'165cm',
        bo:true
    }
}

console.log(obj);







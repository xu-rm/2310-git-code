import ajax from '@/utils/request'

// 获取商品详情
export function getDetailList(id:string){
    return ajax({
        url: '/pro/detail/'+id,
    })
}

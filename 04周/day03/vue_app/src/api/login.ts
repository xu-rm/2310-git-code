import ajax from '@/utils/request'

export function login(data:{loginname:string,password:string}){
    return ajax({
        url:'/user/login',
        method:'POST',
        data
    })
}
# 功能
1. 在文本框内书写内容, 按下回车键以后, 会将输入内容添加在任务列表中展示
2. 左下角有未完成任务统计数字, 实时变化
3. 右下角 “删除所有已完成” 按钮
    - 当没有 已完成 任务的时候隐藏
    - 有 已完成 任务的时候显示
4. 每一个任务的 选项按钮 点击的时候可以切换当前任务的 完成状态
    - 并且和全选按钮联动
5. 下方状态切换按钮的点击, 可以查看部分任务列表
    - all : 查看所有任务
    - active : 查看所有未完成任务
    - completed : 查看所有已完成任务
6. 双击任务名称进入编辑状态
7. 编辑状态内按下 esc 退出编辑状态还原
8. 编辑状态下按下 回车 确认编辑, 修改内容
9. 点击右下角 “删除所有已完成” 按钮时, 将所有已经完成的任务移出列表
10. 点击每一个选项的 “删除” 按钮时, 可以删除当前任务

# 要求: 使用 选项式 API 完成

# 附加题: 给删除和删除已完成 添加一个确认弹框


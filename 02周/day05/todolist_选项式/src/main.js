import { createApp } from "vue";
import App from "./App.vue";
import "./style/base.css";
import "./style/index.css";

const myApp = createApp(App);

myApp.mount("#app");

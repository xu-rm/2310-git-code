import {createStore} from 'vuex'
//创建createStore 函数
const store = createStore({
    state(){
        return {
            num:10086
        }
    },
    mutations:{
        addNum(state){
            state.num++
        }
    }
})

//导出store
export  default store
import {createStore} from 'vuex'
import { stu } from './stu'
const store  = createStore({
    state(){
        return{
            num:1111,
            name:'张三',
        }
    },
    mutations:{
        addNum(state){
            state.num++
        }
    },
    actions:{
        asyncAddNum(context){
            setTimeout(()=>{
                context.commit('addNum')
            },500)
        }
    },
    getters:{
        setName(state){
            return  state.name + '_getter'
        }
    },
    modules:{
        stu
    }
})


export default store
import { createApp } from "vue";
import App from "./App.vue"
// 引入vuex
import store from "./store"

const app = createApp(App)
//挂载store
app.use(store)
app.mount("#app")
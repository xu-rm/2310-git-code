import {createStore} from 'vuex'

const store = createStore({
    state(){
        return{
            list:[]
        }    
    },
    mutations:{
         setList(state,newList){  
            state.list = newList;  
        }
    },
    actions:{
        
    }
})

export default store
import { createApp } from "vue";
import App from "./App.vue";
// 引入vuex
import store from './store'
const myApp = createApp(App);
// 挂载vuex
myApp.use(store)
myApp.mount("#app");

import {createStore} from 'vuex'

const store = createStore({
    state(){
        return {
            num:10086,
            msg:'一个普通的字符串'
        }
    },
    mutations:{
        addNum(state){
            state.num++
        }
    },
    actions:{
       asyncAddNum(context){
        setTimeout(() => {
            context.commit('addNum')
        }, 500);
       }
    }
})

export default store
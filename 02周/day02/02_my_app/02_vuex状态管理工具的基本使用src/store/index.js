import {createStore} from 'vuex'

const store = createStore({
    state(){
        return {
            num:10086,
            msg:'一个普通的字符串'
        }
    },
    mutations:{
        addNum(state,data){
            state.num++
        }
    }
})

export default store
import {createStore} from 'vuex'
import {stu} from './stu'
const store = createStore({
    state(){
        return {
            num:10086,
            msg:'一个普通的字符串'
        }
    },
    getters:{
        // 相当于计算属性
        getMsg(state){
            return state.msg +'_getter'
        }
    },
    modules:{
        stu
    }
})

export default store
import { createApp } from "vue";
import App from "./App.vue";
import store from "./state"
const myApp = createApp(App);
myApp.use(store);
myApp.mount("#app");

import {createRouter,createWebHashHistory} from 'vue-router'

const routes = [
    {
        // 路由地址
        path: "/",
        // 对应路由显示组件
        component: () => import("../views/home.vue"),
    },
    {
        path: "/cart/:id/:name",
        name: "cart",
        component: () => import("@/views/cart.vue"),
    },
    {
        path: "/login",
        component: () => import("../views/login.vue"),
    },
];    

const router = createRouter({
    history:createWebHashHistory(),
    routes
})

export default router
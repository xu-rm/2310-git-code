// 1、引入第三包的一些方法，帮助我们创建一个路由
import {createRouter,createWebHashHistory} from 'vue-router'

const routes = [
    {
        path:'/',
        name:'/',
        component:()=> import("../views/home.vue")
    },
    {
        path:"/cart",
        name:'cart',
        component:()=> import("@/views/cart.vue")
    },
    {
        path:'/login',
        name:'login',
        component:()=> import("@/views/login.vue")
    }
]

// 2、创建一个路由实例
const router = createRouter({
    history:createWebHashHistory(),
    routes
})

// 3、导出路由实例
export default router
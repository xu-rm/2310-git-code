// 1、引入第三包的一些方法，帮助我们创建一个路由
import {createRouter,createWebHashHistory} from 'vue-router'

// import home from "../views/home.vue"
// import cart from "../views/cart.vue"
// import login from "../views/login.vue"

const routes = [
    {
        path:'/',
        name:'/',
        component:()=> import("../views/home.vue")
    },
    {
        path:"/cart",
        name:"cart",
        component:()=> import("@/views/cart.vue"),
    },
    {
        path:'/login',
        name:'login',
        component:()=> import("@/views/login.vue")  
    },{
        path:'/banner',
        name:'banner',
        component:()=> import("@/views/banner/index.vue"),
        children: [
            {
                path:'addBanner',
                name:'addBanner',
                component:()=> import("@/views/banner/components/addBanner.vue")
            },
            {
                path:"delBanner",
                name:"delBanner",
                component:()=> import("@/views/banner/components/delBanner.vue")
            }
        ]      
    }
]


// 2、创建一个路由实例
const router = createRouter({
    history:createWebHashHistory(),
    routes
})

// 添加一个全局前置守卫
router.beforeEach((to,from)=>{
    // to是要去到的页面路由的信息
    // from 是来自哪个页面的路由信息
    console.log("to:  ",to);
    console.log("from:  ",from);
})

router.beforeResolve((to,from)=>{
console.log("to2:  ",to);
console.log("from2:  ",from);
})


// 3、导出路由实例
export default router
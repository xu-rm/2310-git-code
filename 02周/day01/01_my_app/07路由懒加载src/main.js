import { createApp } from "vue";
import App from "./App.vue";
// 1、引入路由
import router from "./router"

const myApp = createApp(App);

// 2、挂载路由实例
myApp.use(router)

myApp.mount("#app");

import ajax from '../utils/request'

// 获取管理员列表
export function getAdminList() {
  return ajax({
    url: '/admin/list'
  })
}

//添加管理员
export function addAdmin(data){
  return ajax({
    url: '/admin/add',
    method: 'post',
    data
  })
}

// 删除管理员
export function delAdmin(data){
  return ajax({
    url:'/admin/delete',
    method: 'post',
    data
  })
}

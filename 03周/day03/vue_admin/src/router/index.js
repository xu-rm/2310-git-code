import { createRouter, createWebHashHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";

const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL),
  routes: [
    //一级路由
    {
      path: "/",
      name: "home",
      component: HomeView,
      //二级路由
      children: [
        {
          path: "manager",
          name: "manager",
          component: () => import("@/views/manager/managerIndexView.vue"),
          //在当前位置书写的对象为三级路由
          children: [
            {
              path: "managerList",
              name: "managerList",
              component: () => import("@/views/manager/managerListView.vue"),
            },
            {
              path: "adminList",
              name: "adminList",
              component: () => import("@/views/manager/adminListView.vue"),
            },
          ],
        },
        {
          path: "banner",
          name: "banner",
          component: () => import("@/views/banner/bannerIndexView.vue"),
          children: [
            {
              path: "bannerList",
              name: "bannerList",
              component: () => import("@/views/banner/bannerListView.vue"),
            },
            {
              path: "addBanner",
              name: "addBanner",
              component: () => import("@/views/banner/addBannerView.vue"),
            },
          ],
        },
      ],
    },
    {
      path: "/login",
      name: "login",
      component: () => import("@/views/loginView.vue"),
    },
  ],
});

export default router;

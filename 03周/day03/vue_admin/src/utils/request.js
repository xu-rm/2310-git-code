import axios from "axios";
import store from '../store'
import router from '../router'
const instance = axios.create({
  baseURL: "http://121.89.205.189:3000/admin",
  timeout: 60000,
});

// 添加请求拦截器
instance.interceptors.request.use(function (config) {
  // 在发送请求之前做些什么
  // console.log(config);
  if(config.url !== '/admin/login'){
    config.headers.token = store.state.userInfo.token
  }
  return config;
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error);
});

// 添加响应拦截器
instance.interceptors.response.use(function (response) {
  // 2xx 范围内的状态码都会触发该函数。
  // 对响应数据做点什么
  // console.log(response);
  if(response.data.code === '10119'){
    router.push('/login')
  }
  return response.data;
}, function (error) {
  // 超出 2xx 范围的状态码都会触发该函数。
  // 对响应错误做点什么
  return Promise.reject(error);
});

export default function ajax(options) {
  const { url, method = "get", data = {} } = options;
  if (/^get$/i.test(method)) {
    return instance.get(url, {
      params: data,
    });
  } else {
    return instance.post(url, data);
  }
}

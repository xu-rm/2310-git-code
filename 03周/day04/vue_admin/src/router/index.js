import { createRouter, createWebHashHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";

export const routes = [
  //一级路由
  {
    path: "/",
    name: "home",
    component: HomeView,
    //二级路由
    children: [
      {
        path: "manager",
        name: "manager",
        label:'人员管理',
        component: () => import("@/views/manager/managerIndexView.vue"),
        //在当前位置书写的对象为三级路由
        children: [
          {
            path: "managerList",
            name: "managerList",
            label:'用户列表',
            component: () => import("@/views/manager/managerListView.vue"),
          },
          {
            path: "adminList",
            name: "adminList",
            label:'管理员列表',
            component: () => import("@/views/manager/adminListView.vue"),
          },
        ],
      },
      {
        path: "banner",
        name: "banner",
        label:'轮播图管理',
        component: () => import("@/views/banner/bannerIndexView.vue"),
        children: [
          {
            path: "bannerList",
            name: "bannerList",
            label:'轮播图列表',
            component: () => import("@/views/banner/bannerListView.vue"),
          },
          {
            path: "addBanner",
            name: "addBanner",
            label:'添加轮播图',
            component: () => import("@/views/banner/addBannerView.vue"),
          },
        ],
      },
    ],
  },
  {
    path: "/login",
    name: "login",
    component: () => import("@/views/loginView.vue"),
  },
]

const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL),
  routes,
});

export default router;

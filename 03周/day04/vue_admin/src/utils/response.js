import { ElMessage } from "element-plus";
export default async function response(code, message, array, callback) {
  if (code !== "200") {
    return ElMessage({
      message: message,
      type: "warning",
    });
  }
  ElMessage({
    message: message,
    type: "success",
  });
  array = (await callback).data;
}

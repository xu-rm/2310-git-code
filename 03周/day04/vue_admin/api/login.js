import ajax from "../";
export function login(data) {
  return ajax({
    url: "/admin/login",
    method: "post",
    data,
  });
}

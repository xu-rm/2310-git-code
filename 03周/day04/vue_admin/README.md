# 01\_项目创建

> npm create vue@latest

```
$ npm create vue@latest

Vue.js - The Progressive JavaScript Framework

√ 请输入项目名称： ... 01_vue_admin                         项目名随意, 不要出现汉字或特殊符号, 最好全字母
√ 是否使用 TypeScript 语法？ ... 否 / 是                    当前项目不需要
√ 是否启用 JSX 支持？ ... 否 / 是                           当前项目不需要
√ 是否引入 Vue Router 进行单页面应用开发？ ... 否 / 是       需要
√ 是否引入 Pinia 用于状态管理？ ... 否 / 是                 当前项目不需要
√ 是否引入 Vitest 用于单元测试？ ... 否 / 是                当前项目不需要
√ 是否要引入一款端到端（End to End）测试工具？ » 不需要         当前项目不需要
√ 是否引入 ESLint 用于代码质量检测？ ... 否 / 是                当前项目不需要

正在构建项目 C:\Users\41099\Desktop\飞秋共享\BK_2310_2\三阶段\03_周\01_vue_admin...

项目构建完成，可执行以下命令：

cd 01_vue_admin         作用: 终端地址进入到项目根目录
npm install             作用: 下载当前项目所需要的依赖
npm run dev             作用: 启动项目
```

# 02\_项目目录说明

1. .vscode
    - 当前编辑器的一些配置文件
2. node_modules
    - 当前项目需要使用的依赖
3. public
    - 当前项目需要使用的一些固定的静态资源(不会发生变化, 主要存放别人处理好的第三方文件)
4. src (重要)
    - assets: 当前项目使用的一些静态资源(这里的静态资源可能会发生变化, 存放自己的静态资源文件)
    - components: 当前项目的公共组件
    - router: 路由相关内容
    - views: 页面文件
    - App.vue: 项目根组件
    - main.js: 项目入口文件
5. .gitignore
    - git 的配置文件
6. index.html
    - 项目的基础文件
7. package-lock.json
    - 依赖包的详细版本与安装地址, 不重要
8. package.json
    - 依赖包的安装版本与项目的一起启动打包等命令
9. README.md
    - 项目的说明书
10. vite.config.js
    - 项目的配置文件

# 03\_项目开发流程

### 公司人员

-   开发人员 (前端, 后端, ios, 安卓)
    -   开发人员配比
        -   3 个前端
        -   5~6 个后端 (有些特殊情况, 后端人员会比较少)
        -   UI/交互/测试 都是一个人 (如果项目工程比较大, 测试人员可能会多两个)
        -   产品 一般都是一个人
-   测试人员
-   设计人员 (UI, 交互)
-   产品经理 (主要负责想一个项目)

### 开发流程

1. 产品需求碰头会
    - 此时产品已经和老板确定了要写什么项目
    - 这个会的主要目的是和开发/测试/UI 确定项目的功能
    - 参会的前端人员一般是 项目的组长+项目的开发人员
    - 特殊情况, 项目比较大的情况, 前端负责人可能也会去
2. 测试碰头会
    - 现在项目功能已经全部确定
    - 此时测试会和开发(前端\_后端)对接测试用例
    - 参会的前端人员是 项目的组长+开发人员
    - 当前会议一定要确保清楚 测试人员测什么
    - 有的时候可能 UI 也会参与测试
        - 如果 UI 比较重要, 会在 测试期 检测项目的 UI 还原度
        - 否则, 可能会等待一个随机时间, 检测随机项目
3. 前后端接口碰头会
    - 现在功能和测试用例已经确认完毕
    - 此时 产品需求文档 和 UI 设计图基本已经设计完毕了
    - 我们需要和后端对接我们功能所需要的参数/数据
        - 将开发的时候可能会遇到对接的参数数据不够, 差一个两个
        - 此时先思考, 能不能利用已经有的数据完成
        - 如果确实完成不了, 那么再找后端 添加一个新的数据
4. 前端组内会议
    - 一般就是开发人员比较多的时候开会
    - 主要是分配一下每个人, 负责的功能
5. 开发期 (1 天/3 天/5 天/10 天)
    - 此时是前端后端同时开发
    - 所以前端没有真实后端接口
    - 后端没有真实前端页面
    - 所以前端此时使用的基本都是 mock 假数据
6. 前后端接口联调 (1 天/3 天)
    - 前端后端功能已经写完了(其实完成了 90%)
    - 开始对接 前端后端接口是否有问题
    - 此时大部分问题, 可能会处在后端上
7. 测试期
    - 此时前端功能必须 100% 完毕
    - 此时前端后端就等着 测试指定 bug, 一般会通过 禅道
    - 有些公司会在 项目完成后 进行 代码复盘/禅道问题统计
8. 项目上线
    - 上线日期一般都是提前定好的
    - 上线的时间一般都是 22:00 以后
    - 所以前端需要留下一名开发者, 解决上线过程中可能会出现的问题

# 引入 UI 组件库

> UI 组件库是面向 UI 设计师和前端开发人员使用的
> 提供了一些样式与一些基本的功能, 但是详细的 JS 还需要我们自己完成

-   当前项目使用 ElementPlus
    -   官网地址: `https://element-plus.gitee.io/zh-CN/`
    -   如果将来项目中 vue 版本是 2 版本
    -   那么需要使用 ElementUi
        -   官网地址: `https://element.eleme.io/#/zh-CN`
-   ElementPlus 安装命令
    -   `npm install element-plus --save`

### 用法

##### 1. 完整引入 (不推荐, 了解即可)

-   相当于 将 elementPlus 提供的所有组件, 全部注册成全局组件
-   使用的时候比较方便, 但是项目书写完毕后, 打包的时候 项目的文件大小会比较大

```js
// main.js
import { createApp } from "vue";
import ElementPlus from "element-plus";
import "element-plus/dist/index.css";
import App from "./App.vue";

const app = createApp(App);

app.use(ElementPlus);
app.mount("#app");
```

##### 2. 按需导入

> 首先你需要安装 unplugin-vue-components 和 unplugin-auto-import 这两款插件
> 然后把下列代码加入到我们的项目中

###### Vite (我们当前项目需要使用)

```js
// vite.config.js
import { defineConfig } from "vite";
import AutoImport from "unplugin-auto-import/vite";
import Components from "unplugin-vue-components/vite";
import { ElementPlusResolver } from "unplugin-vue-components/resolvers";

export default defineConfig({
    // ...
    plugins: [
        // ...
        AutoImport({
            resolvers: [ElementPlusResolver()],
        }),
        Components({
            resolvers: [ElementPlusResolver()],
        }),
    ],
});
```

###### Webpack (当前项目不需要使用, 了解即可)

```js
// webpack.config.js
const AutoImport = require("unplugin-auto-import/webpack");
const Components = require("unplugin-vue-components/webpack");
const { ElementPlusResolver } = require("unplugin-vue-components/resolvers");

module.exports = {
    // ...
    plugins: [
        AutoImport({
            resolvers: [ElementPlusResolver()],
        }),
        Components({
            resolvers: [ElementPlusResolver()],
        }),
    ],
};
```

##### 3. 手动导入

```vue
<!-- App.vue -->
<template>
    <el-button>我是 ElButton</el-button>
</template>
<script>
import { ElButton } from "element-plus";
export default {
    components: { ElButton },
};
</script>
```

```js
// vite.config.js
import { defineConfig } from "vite";
import ElementPlus from "unplugin-element-plus/vite";

export default defineConfig({
    // ...
    plugins: [ElementPlus()],
});
```

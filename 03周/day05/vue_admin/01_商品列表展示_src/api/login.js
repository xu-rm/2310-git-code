import ajax from "../utils/request";
export function loginRequest(data) {
  return ajax({
    url: "/admin/login",
    method: "post",
    data,
  });
}

import axios from "axios";
const instance = axios.create({
  baseURL: "http://121.89.205.189:3000/admin",
  timeout: 60000,
});

export default function ajax(options) {
  const { url, method = "get", data = {} } = options;
  if (/^get$/i.test(method)) {
    return instance.get(url, {
      params: data,
    });
  } else {
    return instance.post(url, data);
  }
}

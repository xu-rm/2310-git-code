import ajax from '@/utils/request';

//添加轮播图
export function addBanner(data){
    return ajax({
        url:'/banner/add',
        method:'post',
        data
    })
}

// 获取轮播图列表
export function getBannerList(){
    return ajax({
       url:'/banner/list' 
    })
}

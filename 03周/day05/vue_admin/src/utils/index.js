import { ElMessage } from "element-plus";
export function myMessage(code, message,callback) {
  if (code !== "200") {
    return ElMessage({
      type: "warning",
      message
    });
  }
  ElMessage({
    type: "success",
    message
  });
  callback()
}

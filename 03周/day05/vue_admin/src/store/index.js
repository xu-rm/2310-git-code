import { createStore } from "vuex";
import createPersistedState from "vuex-persistedstate";
const store = createStore({
  state() {
    return {
      userInfo: {},
    };
  },
  mutations: {
    setUserInfo(state, info) {
      state.userInfo = info;
    },
  },
  plugins: [createPersistedState()],
});

export default store;

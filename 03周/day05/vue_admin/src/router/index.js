import { createRouter, createWebHashHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";

export const routes = [
  //一级路由
  {
    path: "/",
    name: "home",
    component: HomeView,
    //二级路由
    children: [
      {
        path: "manager",
        name: "manager",
        label: "人员管理",
        component: () => import("@/views/manager/managerIndexView.vue"),
        //在当前位置书写的对象为三级路由
        children: [
          {
            path: "managerList",
            name: "managerList",
            label: "用户列表",
            component: () => import("@/views/manager/managerListView.vue"),
          },
          {
            path: "adminList",
            name: "adminList",
            label: "管理员列表",
            component: () => import("@/views/manager/adminListView.vue"),
          },
        ],
      },
      {
        path: "banner",
        name: "banner",
        label: "轮播图管理",
        component: () => import("@/views/banner/bannerIndexView.vue"),
        children: [
          {
            path: "bannerList",
            name: "bannerList",
            label: "轮播图列表",
            component: () => import("@/views/banner/bannerListView.vue"),
          },
          {
            path: "addBanner",
            name: "addBanner",
            label: "添加轮播图",
            component: () => import("@/views/banner/addBannerView.vue"),
          },
        ],
      },
      {
        path: "echarts",
        name: "echarts",
        label: "图表展示",
        component: () => import("@/views/echarts/echartsIndexView.vue"),
        children: [
          {
            path: "echartsshow",
            name: "echartsshow",
            label: "echarts展示",
            component: () => import("@/views/echarts/echartsShowView.vue"),
          },
        ],
      },
      {
        path: "excel",
        name: "excel",
        label: "文件处理",
        component: () => import("@/views/excel/excelIndexView.vue"),
        children: [
          {
            path: "excelimport",
            name: "excelimport",
            label: "excel导入",
            component: () => import("@/views/excel/excelImportView.vue"),
          },
          {
            path: "excelexport",
            name: "excelexport",
            label: "excel导出",
            component: () => import("@/views/excel/excelExportView.vue"),
          },
        ],
      },
      {
        path: "map",
        name: "map",
        label: "地图展示",
        component: () => import("@/views/map/mapIndexView.vue"),
        children: [
          {
            path: "mapshow",
            name: "mapshow",
            label: "百度地图展示",
            component: () => import("@/views/map/mapShowView.vue"),
          },
        ],
      },
      {
        path: "editor",
        name: "editor",
        label: "富文本编辑器",
        component: () => import("@/views/editor/editorIndexView.vue"),
        children: [
          {
            path: "editorshow",
            name: "editorshow",
            label: "编辑器",
            component: () => import("@/views/editor/editorShowView.vue"),
          },
        ],
      },
      {
        path: "proList",
        name: "proList",
        label: "门店管理",
        component: () => import("@/views/proList/proListIndexView.vue"),
        children: [
          {
            path: "proListShow",
            name: "proListShow",
            label: "门店列表",
            component: () => import("@/views/proList/proListShowView.vue"),
          },
        ],
      },
      
    ],
  },
  {
    path: "/login",
    name: "login",
    component: () => import("@/views/loginView.vue"),
  },
];

const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL),
  routes,
});

export default router;

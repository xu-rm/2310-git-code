// import './assets/main.css'

import { createApp } from 'vue'

import App from './App.vue'

// 引入组件
import my_inp from './components/my_inp.vue'

const myApp = createApp(App)
//注册一个全局组件，将来这个组件可以在项目中的任何一个位置使用
myApp.component('my_inp',my_inp)

myApp.mount('#app')
